from netmiko import ConnectHandler
from lxml import etree
from itertools import groupby
import logging

from mimir.devices.generic import NetworkDevice


def xml2dict(e):
    """
    Convert an etree derived from keymile XML output
    into a dict structure which our API can return
    @type  e: etree.Element
    @param e: the root of the tree
    @return: The dictionary representation of the XML tree
    """

    def _xml2dict(e):
        kids = {}

        # handle attributes
        if e.attrib:
            if e.attrib != {}:
                kids = {**kids, **e.attrib}

        # handle content (in brackets)
        if e.text:
            e.text = e.text.strip()
            if e.text != "":
                return e.text

        # handle children
        for k, g in groupby(e):
            for x in g:
                tmp = _xml2dict(x)
                if k.tag in kids:
                    if type(kids[k.tag]) is not list:
                        kids[k.tag] = [kids[k.tag]]
                    kids[k.tag].append(tmp)
                else:
                    kids[k.tag] = tmp
        return kids

    return {e.tag: _xml2dict(e)}


class KeymileNetworkDevice(NetworkDevice):
    def connect(self):
        # Milegate cli output termination byte
        self.expect_string = "\x03"

        self.connection = ConnectHandler(**self.netmiko_settings)
        if self.use_xml is True:
            # The Keymile shell is dumping plain XML
            # if you're asking nicely. Let's use that.

            self.connection.send_command(
                "mode etx on",
                expect_string=self.expect_string,
                strip_prompt=False,
                strip_command=True,
            )
            self.connection.send_command(
                "mode display xml",
                expect_string=self.expect_string,
                strip_prompt=False,
                strip_command=False,
            )

        logging.info(
            "Connected. host:{} logic:{} user:'{}', device_type:'keymile' use_xml:{}".format(
                self.host,
                self.name,
                self.netmiko_settings.get("username"),
                self.use_xml,
            )
        )
        return self.connection.find_prompt()

    def send_command(self, command):

        # XML mode
        if self.use_xml is True:
            # we enabled the termination byte in connect()
            # so now we're searching for that with expect_string
            try:
                output = self.connection.send_command(
                    command,
                    expect_string=self.expect_string,
                    strip_prompt=False,
                    strip_command=False,
                )
            except (ValueError, OSError) as e:
                success = False
                output = "{}: {}".format(e.__class__.__name__, str(e).strip())
                logging.warn(
                    "Error. host:{} logic:{} command:'{}' error:'{}'".format(
                        self.host, self.name, command, str(e)
                    )
                )

            # in XML mode keymile device doesnt print the command/input but
            # the output has a prepending empty line, so we strip it
            output = output[1:]

            # remove except_string/termination byte from output
            output = output.rstrip(self.expect_string)

            # parse xml / convert to dict
            output = self.convert_xml(output)

            if output is None:
                output = []
                success = False
                log_str = "[conversion of xml failed]"
            else:
                success = self.validate_xml(output)
                log_str = "[xml content]"

        # plain mode
        else:
            output = self.connection.send_command(command)
            output = output.splitlines()
            log_str = (
                "{}... [{} line]".format(str(output)[:40], len(output))
                if len(output) > 1
                else str(output)
            )
            success = self.validate(output)

            logging.info(
                "Finished. host:{} logic:{} command:'{}' use_xml:{} success:{} output:'{}'".format(
                    self.host, self.name, command, self.use_xml, success, log_str
                )
            )
        return success, output

    def convert_xml(self, data):
        """Output validation based on XML content"""
        data = data.encode("ascii")
        try:
            xml_data = etree.fromstring(data)
            data = xml2dict(xml_data)
        except etree.XMLSyntaxError as e:
            logging.error(
                "XML Parsing error. host:{} logic:{} msg:'{}'".format(
                    self.host, self.name, str(e)
                )
            )
            return None

        # relevant data is in the key 'response'
        return data.get("response", None)

    def validate_xml(self, data):
        # check if commands were executed successfully
        operation_execution_status = (
            data.get("operation", {}).get("execution", {}).get("status")
        )
        logging.debug(
            "XML validation. host:{} logic:{} success:'{}'".format(
                self.host,
                self.name,
                "True" if (operation_execution_status == "success") else "False",
            )
        )
        return True if (operation_execution_status == "success") else False
