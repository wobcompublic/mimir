from pydantic import BaseModel, Schema


class PingResponse(BaseModel):
    detail: str = Schema(None, description="Detailed status message")
    prompt: str = Schema(None, description="Shell prompt")
    execution_time: str = Schema(
        None, description="Time passed to serve the request in seconds"
    )


class Error(BaseModel):
    detail: str = Schema(None, description="Detailed error message")


Codes = {
    200: {"model": PingResponse, "description": "Ping successfull."},
    201: {"model": PingResponse, "description": "Ping failed."},
}
