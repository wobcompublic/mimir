# mimir - network device gateway

This is the repository of **mimir**, a network device web API developed by [WOBCOM GmbH](https://www.wobcom.de/).

You can use it to seamlessly integrate shell interactions with network equipment into your [Operational Support System](https://en.wikipedia.org/wiki/Operations_support_system), Continous Integration or into other Provisioning Systems via HTTP.

![/execute endpoint](screenshot1.png "/execute endpoint") 

It provides methods to execute remote commands via SSH and evaluate their return codes, even if the device only supports *interactive* shell sessions (see [SSH RFC 4254 6.5](https://tools.ietf.org/html/rfc4254#page-13) for more information on this topic).


## Features

- Simple blocking / synchronous HTTP API 
- OpenAPI documentation of methods
- Support for over 50 types of devices and operating systems via netmiko library ([list of supported devices](https://github.com/ktbyers/netmiko)).
- Ability to parse the console output of the commands to structured data via [TextFSM](https://pynet.twb-tech.com/blog/automation/netmiko-textfsm.html)
- Ability to easily extend device types with custom logic
- Extended feature for KeyMile MileGate 2300 devices: By using parameter `use_xml`, the execute method will return the commands answer in a structure derived from the shells XML mode (CLI: `mode display xml`). This is a super convenient alternative to TextFSM.


## Documentation

**Ressource:** /api/v1/device/{hostname}/execute

**Payload:** application/json 

```
{
  "username": "",
  "password": "",
  "device_type": "",
  "port": 22,
  "timeout": 10,
  "auth_timeout": 15,
  "delay_factor": 1,
  "commands": [
    "show version"
  ],
  "enable": false,
  "configure": false,
  "secret": "",
  "stop_on_first_error": true,
  "error_indicators": [
    "Error:",
    "error:",
    "Invalid",
    "invalid token",
    "Login incorrect",
    "can't be added",
    "already exists"
  ],
  "use_textfsm": false,
  "use_xml": false,
  "debug": false
}
```

**Response:**

```
{
  "detail": "string",
  "commands": [
    "string"
  ],
  "outputs": [
    {
      "success": bool,
      "detail": "string",
      "command": "string",
      "output": [
        [
          "string"
        ]
      ],
      "execution_time": "string"
    }
  ],
  "execution_time": "string"
}
```

You can find a complete and interactive documentation and test-suite included with the service at 

  * [https://mimir-server/api/v1/docs](https://mimir/api/v1/docs) for SwaggerUI
  
  * [https://mimir/redoc](https://mimir/redoc) for redoc


## Deployment 

via [DOCKER](DOCKER.md)

## Frameworks and Libraries used

- [python3](https://www.python.org/), PSF license
- [netmiko](https://github.com/ktbyers/netmiko), MIT license
- [fastapi](https://fastapi.tiangolo.com/), MIT license
- [redis](https://redis.io/), MIT license
- [TextFSM](https://github.com/google/textfsm), Google CLA.

## Changes

see [CHANGES](CHANGES.md)

## License

This code is distributed under the GPLv3 license, see the [LICENSE](LICENSE.md) file.

## Authors

Copyright (c) <2019> Thomas Karmann <thomas.karmann@port-zero.com>
