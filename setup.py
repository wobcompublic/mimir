#!/usr/bin/env python

from distutils.core import setup

setup(
    name="mimir",
    version="0.2",
    description="Web gateway for interaction with network devices via SSH.",
    author="Thomas Karmann",
    author_email="thomas.karmann@port-zero.com",
    url="https://gitlab.com/wobcom/mimir/",
    packages=["mimir", "mimir.devices", "mimir.common", "mimir.actions", "mimir.actions.ping", "mimir.actions.execute"],
)
