from typing import List, Set
from pydantic import BaseModel, Schema


class CommandStatus(BaseModel):
    success: bool = Schema(None, description="Success status of a command")
    detail: str = Schema(None, description="Detailed exit status message")
    command: str = Schema(None, description="Command that was executed")
    output: List[Set[str]] = Schema(
        None, description="Output of command, line by line, as a list of strings"
    )
    execution_time: str = Schema(
        None, description="Time passed to serve the request in seconds"
    )


class ExecuteResponse(BaseModel):
    detail: str = Schema(None, description="Detailed status message")
    commands: List[str] = Schema(None, description="List of executed commands")
    outputs: List[CommandStatus] = Schema(
        None, description="List of outputs of commands"
    )
    execution_time: str = Schema(
        None, description="Time passed to serve the request in seconds"
    )


class Error(BaseModel):
    detail: str = Schema(None, description="Detailed error message")


Codes = {
    200: {
        "model": ExecuteResponse,
        "description": "Connection to device successful and all commands succeeded",
    },
    201: {
        "model": ExecuteResponse,
        "description": "Connection to device successful but commands failed.",
    },
    422: {"model": Error, "description": "Service Parameter Error."},
    500: {
        "model": Error,
        "description": "Internal Server Error: Connection to redis broker failed.",
    },
    501: {"model": Error, "description": "Specified parameter is not supported."},
    502: {"model": Error, "description": "Error in SSH connection."},
    503: {"model": Error, "description": "Already communicating with device."},
    504: {
        "model": Error,
        "description": "Timeout. No response from the network device.",
    },
    508: {"model": Error, "description": "Could not find prompt."},
    511: {
        "model": Error,
        "description": "Authentication to the network device failed.",
    },
}
