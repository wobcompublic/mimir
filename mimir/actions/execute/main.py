#!venv/bin/python
import logging

from fastapi import APIRouter, HTTPException
from starlette.responses import JSONResponse
from netmiko import NetMikoTimeoutException, NetMikoAuthenticationException
from textfsm import TextFSMTemplateError
from paramiko.ssh_exception import SSHException
import redis
from redis.exceptions import LockNotOwnedError

from mimir.definitions import CONFIG_PATH
from mimir.common.util import Timer, set_logfile
from mimir.common.settings import Settings

from mimir.actions.helpers import get_driver_by_device_type
from mimir.actions.execute.parameters import ExecuteParameters
from mimir.actions.execute.responses import ExecuteResponse, Codes

redis_meta = Settings("{}/mimir.conf".format(CONFIG_PATH), "redis").as_dict()
router = APIRouter()


@router.post(
    "/api/v1/device/{hostname}/execute",
    summary="Execute a list of commands on a network device via SSH",
    tags=["Actions"],
    response_class=JSONResponse,
    response_model=ExecuteResponse,
    response_description="Connection to device successful and all commands succeeded",
    responses=Codes,
)
def execute(*, hostname: str, params: ExecuteParameters):
    set_logfile(hostname, logging.DEBUG if params.debug else logging.INFO)
    timer = Timer()
    timer.tick()

    # pass through connection parameters to netmiko handler
    connection_settings = {
        "host": hostname,
        "device_type": params.device_type,
        "port": params.port,
        "username": params.username,
        "password": params.password,
        "secret": params.secret,
        "timeout": params.timeout,
        "auth_timeout": params.auth_timeout,
        "global_delay_factor": params.delay_factor,
    }

    # dynamic import extension
    driver = get_driver_by_device_type(params.device_type)

    # connect to redis server to acquire a lock for the device
    redis_host = redis_meta.get("host", "localhost")
    redis_port = redis_meta.get("port", "6379")
    lock_timeout = (
        # no hard timeout for now...
        None
        # lock for as long as the session is allowed to live plus some
        # extra time to clean up
        # params.timeout + 6
    )
    logging.debug(
        "Connect to redis server host:{} redis_server:{}:{}.".format(
            hostname, redis_host, redis_port
        )
    )
    lock = False
    try:
        my_lock = redis.Redis(host=redis_host, port=redis_port).lock(
            "{}_{}".format("mimir", hostname), timeout=lock_timeout, blocking_timeout=10
        )
    except ConnectionError as e:
        logging.error("Could not connect to redis server. host:{}".format(hostname))
        raise HTTPException(
            status_code=500, detail="Internal Server Error: {}".format(str(e))
        )

    success = False
    try:
        logging.debug("Trying to acquire lock. host:{}".format(hostname))
        lock = my_lock.acquire(blocking=False)
        if lock:
            logging.debug("Locked. host:{}".format(hostname))
        else:
            logging.info(
                "Service already communicating with the device. host:{}".format(
                    hostname
                )
            )
            raise HTTPException(
                status_code=503,
                detail="Service Unavailable: Already communicating with the device.",
            )

        # create class instance, connect, execute commands, disconnect
        try:
            # call device abstraction
            args = (
                connection_settings,
                params.use_textfsm,
                params.use_xml,
                params.error_indicators,
            )
            kw = {}
            device = driver(*args, **kw)
            device.connect()

            # execute commands
            results, success = device.send_command_list(
                params.commands,
                params.enable,
                params.configure,
                params.stop_on_first_error,
            )

            # disconnect ssh session
            device.disconnect()
        except (
            NetMikoTimeoutException
        ) as e:  # bad gateway / no connection to network device
            logging.debug("Error. host:{} msg:'{}'".format(hostname, str(e)))
            raise HTTPException(status_code=504, detail=str(e))
        except (NetMikoAuthenticationException) as e:  # auth failure
            logging.debug("Error. host:{} msg:'{}'".format(hostname, str(e)))
            raise HTTPException(status_code=511, detail=str(e).replace("\n", " "))
        except (SSHException) as e:  # ssh connection errors
            logging.debug("Error. host:{} msg:'{}'".format(hostname, str(e)))
            raise HTTPException(status_code=502, detail=str(e))
        except (ValueError) as e:  # bad netmiko connection parameter or value
            logging.debug("Error. host:{} msg:'{}'".format(hostname, str(e)))
            raise HTTPException(status_code=501, detail=str(e).replace("\n", " "))
        except (TextFSMTemplateError) as e:
            logging.debug("Error. host:{} msg:'{}'".format(hostname, str(e)))
            raise HTTPException(
                status_code=505,
                detail="TextFSM Template Error: {}".format(
                    e.__class__.__name__, str(e)
                ),
            )
    except redis.exceptions.ConnectionError as e:
        logging.error("Redis Connection Error. msg:'{}'".format(hostname, e))
        raise HTTPException(
            status_code=500, detail="Internal Server Error: {}".format(str(e))
        )

    # release lock
    finally:
        if lock and my_lock:
            try:
                my_lock.release()
            except LockNotOwnedError as e:
                logging.debug(
                    "Problems releasing lock. host:{} {}, {}, {}.".format(
                        hostname, str(e), str(lock), str(my_lock)
                    )
                )
            except redis.exceptions.ConnectionError as e:
                raise HTTPException(
                    status_code=500, detail="Internal Server Error: {}".format(str(e))
                )
            logging.debug("Lock released. host:{}".format(hostname))

    return JSONResponse(
        status_code=200 if success else 201,
        content={
            "detail": "Connection to device successful and all commands succeeded"
            if success
            else "Connection to device successful but commands failed.",
            "commands": params.commands,
            "outputs": results,
            "execution_time": timer.tock(),
        },
    )
