FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

LABEL maintainer="Thomas Karmann <thomas.karmann@port-zero.com>"

# Copy Sourcetree / Install Application
COPY . /app
WORKDIR /app

# Update pip
RUN pip3 install --upgrade pip

# Install runtime dependencies
RUN pip3 install -r requirements.txt

# new pwd
WORKDIR /app

# download textfsm templates
RUN make textfsm
ENV NET_TEXTFSM ${pwd}/ntc-templates/templates

# new pwd
WORKDIR /app/mimir

# create directory for logging
RUN mkdir -p /var/log/mimir

RUN apt-get update
RUN apt-get install --yes logrotate

# logrotate
COPY scripts/logrotate /etc/logrotate.d/mimir
