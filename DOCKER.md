# Deployment via GitLab CI and Docker

## Containers used

* [uvicorn-gunicorn-fastapi-docker](https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker) (for python3.7) Provides a uvicorn webserver interface for Python.
* [traefik](https://hub.docker.com/_/traefik) (= 1.7.x) Used for SSL Termination and HTTP Basic Auth.
* [redis](https://hub.docker.com/_/redis) (redis:4.0.5-alpine) Used for synchronizing the resources or multiple instances. 

## GitLab CI

1. In GitLab CI Configuration "General Pipelines" set your Custom CI config path to  ```gitlab-ci-docker-deployment.yml```.

## Configure production and staging systems

*For each system do the following steps:*

1. Create a system user with privileges to run docker (e.g. add to group ```docker```).

2. Create an SSH key pair for the user.

3. Copy & paste Private Key to GitLab CI Environment Variable: ```SSH_PRIVATE_KEY```

4. Copy & paste Public Key to target users ```/home/user/.ssh/.authorized_keys```

5. Set CI Environment Variables:
  - ```SSH_USER```
  - ```DEPLOY_SERVER_STAGING``` and/or ```DEPLOY_SERVER_PRODUCTION```
  - ```SSH_KNOWN_HOSTS``` 


## Configure SSL Certificate

*Inside mimir sources:*

1.  Get Gertificate

    1.1 Use self-signed dummy certificates:
    ```
    % scripts/create_self_signed_certificates.sh
    ```
    *Two files will be generated: ssl/cert.key and ssl/cert.crt*

    1.2 Use existing certificates:
    - Put the key to ```ssl/cert.key```
    - Put the cert to ```ssl/cert.crt```

2. Set CI Environment Variables:
  - ```CERTIFICATE_KEY```: paste content of cert.key
  - ```CERTIFICATE_CRT```: paste content of cert.crt


## Configure HTTP Basic Auth Credentials

*Inside mimir sources:*

1. Create Password String: 
```
% scripts/create_basic_auth_string.sh username password
```

2. Paste string as value of CI Environment Variables:

  - `HTTP_BASIC_AUTH_PRODUCTION` 
  - `HTTP_BASIC_AUTH_STAGING`
  
Changes will be applied after traefik image has been rebuilt and restarted (e.g. via ```docker-compose down && docker-compose up```).



