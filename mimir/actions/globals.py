from pydantic import BaseModel, Schema
from netmiko.ssh_dispatcher import CLASS_MAPPER_BASE

netmiko_vendors = ", ".join([*CLASS_MAPPER_BASE])


class NetmikoParameters(BaseModel):
    username: str = Schema(
        "", example="", description="Username to login as", min_length=1, max_length=256
    )
    password: str = Schema(
        "",
        example="",
        description="Password to login with",
        min_length=1,
        max_length=256,
    )
    device_type: str = Schema(
        "",
        example="",
        description="Type of the network device. Available: {}".format(netmiko_vendors),
        min_length=1,
        max_length=256,
        # regexp="^keymile$"
    )
    port: int = Schema(
        22,
        example=22,
        ge=1,
        le=32767,
        description="The destination port used to connect to the network device.",
    )
    timeout: int = Schema(
        10,
        example=10,
        ge=1,
        description="After inactivity of timeout seconds the session will be closed.",
    )
    auth_timeout: int = Schema(
        15,
        example=15,
        ge=1,
        description="Initial Timeout to establish connection in seconds.",
    )
    delay_factor: int = Schema(
        1.0,
        example=1.0,
        ge=0.25,
        le=10.0,
        description="Netmiko connection parameter 'global_delay_factor'.",
    )
