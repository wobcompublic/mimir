import configparser
import json


class InvalidSettingsFile(Exception):
    pass


def remove_quotes(original):
    d = original.copy()
    for key, value in d.items():
        if isinstance(value, str):
            s = d[key]
            if s.startswith(('"', "'")):
                s = s[1:]
            if s.endswith(('"', "'")):
                s = s[:-1]
            d[key] = s
        if isinstance(value, dict):
            d[key] = remove_quotes(value)
    return d


class Settings:
    def __init__(self, settings_ini, section="netmiko_settings"):
        self.settings_ini = settings_ini
        self.config = configparser.ConfigParser()
        self.config.read(settings_ini)

        try:
            self.d = self.to_dict(dict(self.config.items(section)))
        except configparser.NoSectionError:
            raise InvalidSettingsFile(
                "Settings file '{}' is missing section '{}'".format(
                    settings_ini, section
                )
            )

    def as_dict(self):
        return self.d

    def to_dict(self, config):
        """
        Nested OrderedDict to normal dict.
        Also remove quotes around string values.
        """
        d = json.loads(json.dumps(config))
        d = remove_quotes(d)
        return d
