from typing import List
from pydantic import Schema

from mimir.actions.globals import NetmikoParameters


class ExecuteParameters(NetmikoParameters):
    commands: List[str] = Schema(
        ["show version"],
        example=["show version"],
        description="Commands to execute on the device in batch mode",
    )
    enable: bool = Schema(
        False,
        example=False,
        description="Switch to priviliged mode. An implicit 'enable' will be "
        "executed immediately after login.",
    )
    configure: bool = Schema(
        False,
        example=False,
        description="Execute commands in configure mode. An implicit"
        "'configure terminal' command is prepended. Commands will be executed"
        "in batch and therefore not evaluated individually.",
    )
    secret: str = Schema(
        "",
        example="",
        description="The enable password if target device requires one.",
        max_length=256,
    )
    stop_on_first_error: bool = Schema(
        True, example=True, description="Stop execution on first error."
    )
    error_indicators: List[str] = Schema(
        [
            "Error:",
            "error:",
            "Invalid",
            "invalid token",
            "Login incorrect",
            "can't be added",
            "already exists",
        ],
        example=[
            "Error:",
            "error:",
            "Invalid",
            "invalid token",
            "Login incorrect",
            "can't be added",
            "already exists",
        ],
        description="Strings in the Output that will make the validation logic fail.",
    )
    use_textfsm: bool = Schema(
        False,
        example=False,
        description="Parse output to structured data via TextFSM Templates",
    )
    use_xml: bool = Schema(
        False,
        example=False,
        description="Only for Keymile NOS: receive shell output in XML and convert to JSON",
    )
    debug: bool = Schema(
        False, example=False, description="Switch log to debug verbosity."
    )
