# Changes

## 0.4
- Feature: Support for enable mode
- Feature: Full support for configure mode and commands
- Feature: Add param error_indicators for better control over validation
- Refactor: moved command dispatching code to devices/
- Refactor: router hierachy for more easy modifications
- Removed: parameter override by file
- Cleanup: Logging
- Cleanup: setup.py
- Updated: dependencies

## 0.3
- Support for Keymile Milegate 2300 (updated netmiko layer)
- Feature: multi-pattern error detection for generic devices
- Feature: support for TextFSM templates to parse text to dicts
- Bugfix: added missing exception handing for netmiko connect
- Improvements build system
- Improvements deployment

## 0.2
- Passthrough additional parameters to netmiko: secret, port
- New feature: `stop_on_first_error` parameter which closes the session on first failing command

## 0.1
- Basic featureset
- /execute ressource

