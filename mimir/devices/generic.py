#!venv/bin/python

import logging

from netmiko import ConnectHandler

from mimir.common.util import Timer


class NetworkDevice(object):
    def __init__(
        self,
        netmiko_settings,
        use_textfsm=False,
        use_xml=False,
        error_indicators=[
            "Error:",
            "error:",
            "Invalid",
            "invalid token",
            "Login incorrect",
            "can't be added",
            "already exists",
        ],
    ):
        """
        Connect to the device.
        @type settings: dict
        @param settings: netmiko settings
        @return: None
        """
        self.set_name()
        self.device_type = netmiko_settings.get("device_type")
        self.host = netmiko_settings.get("host")
        self.use_textfsm = use_textfsm
        self.use_xml = use_xml
        self.error_indicators = error_indicators
        self.netmiko_settings = netmiko_settings
        self.connection = None

    def connect(self):
        logging.info(
            "Login... host:{} logic:{} user:'{}'".format(
                self.host, self.name, self.netmiko_settings.get("username")
            )
        )

        self.connection = ConnectHandler(**self.netmiko_settings)
        logging.info(
            "Connected. host:{} logic:{} user:'{}' device_type:'{}'".format(
                self.host,
                self.name,
                self.netmiko_settings.get("username"),
                self.device_type,
            )
        )
        return self.connection.find_prompt()

    def set_name(self):
        # name scheme: VendorNetworkDevice
        self.name = self.__class__.__name__.replace("NetworkDevice", "").lower()

    def send_command(self, command):
        """
        @type command: string
        @param command: command to execute on device
        @type success: bool
        @return success: indicates successful execution of command
        @type output: list
        @return output: individual lines of terminal output of command
        """
        output = self.connection.send_command(command, use_textfsm=self.use_textfsm)

        if self.use_textfsm is True:
            success = True if isinstance(output, (list, dict)) else False
            log_str = str(output)
        else:
            output = output.splitlines()
            log_str = (
                "{}... [{} lines]".format(str(output[0]), len(output))
                if len(output) > 1
                else str(output)
            )
            success = self.validate(output)

        logging.info(
            "Finished. host:{} logic:{} command:'{}' success:{} output:'{}'".format(
                self.host, self.name, command, success, log_str
            )
        )
        return success, output

    def send_config_command(self, command):
        """
        @type command: string
        @param command: command to execute on device
        @return success: bool which indicates successful execution of command
        @return output: terminal output of command
        """
        output = self.connection.send_command(
            command, use_textfsm=self.use_textfsm, auto_find_prompt=False
        )
        # so we check for type
        if self.use_textfsm is True:
            success = True if isinstance(output, (list, dict)) else False
            log_str = str(output)
        else:
            output = output.splitlines()
            log_str = (
                "{}... [{} lines]".format(str(output[0]), len(output))
                if len(output) > 1
                else str(output)
            )
            success = self.validate(output)

        logging.info(
            "Finished. host:{} logic:{} command:'{}' success:{} output:'{}'".format(
                self.host, self.name, command, success, log_str
            )
        )
        return success, output

    def send_command_list(self, commands, enable, configure, stop_on_first_error):
        results = []
        timer = Timer()

        if enable:
            self.connection.enable()
            logging.info("Enable. host:{} logic:{}".format(self.host, self.name))

        logging.info(
            "Execute. host:{} logic:{} commands:'{}' stop_on_first_error:{}".format(
                self.host, self.name, commands, stop_on_first_error
            )
        )
        if configure:
            # switch to config mode
            self.connection.config_mode()

            for command in commands:
                timer.tick()
                try:
                    success, output = self.send_config_command(command)
                except (ValueError, OSError) as e:
                    success = False
                    output = "{}: {}".format(e.__class__.__name__, str(e).strip())
                    logging.warn(
                        "Configure. host:{} logic:{} command:'{}' error:'{}'".format(
                            self.host, self.name, command, str(e)
                        )
                    )

                # add to results
                results.append(
                    {
                        "command": command,
                        "success": success,
                        "output": output,
                        "execution_time": timer.tock(),
                    }
                )

                # cancel execution if stop_on_first_error was requested!
                if stop_on_first_error is True:
                    if success is False:
                        break

            # leave config mode
            self.connection.exit_config_mode()
        else:

            # step through commands, execute and collect results
            for command in commands:
                timer.tick()
                try:
                    success, output = self.send_command(command)
                except (ValueError, OSError) as e:
                    success = False
                    output = "{}: {}".format(e.__class__.__name__, str(e).strip())
                    logging.warn(
                        "Error. host:{} logic:{} command:'{}' error:'{}'".format(
                            self.host, self.name, command, str(e)
                        )
                    )

                results.append(
                    {
                        "command": command,
                        "success": success,
                        "output": output,
                        "execution_time": timer.tock(),
                    }
                )

                # cancel execution if stop_on_first_error was requested!
                if stop_on_first_error is True:
                    if success is False:
                        break

        logging.info(
            "Finished. host:{} logic:{} success:{}".format(
                self.host,
                self.name,
                all([_command.get("success") for _command in results]),
            )
        )
        return results, all([_command.get("success") for _command in results])

    def validate(self, data):
        """
        Output validation based on substring search
        @type data: list(str)
        @param data: text/output to validate using device specific logic
        @return: bool indicating success or failure
        """
        for line in data:
            if any(indicator in line for indicator in self.error_indicators):
                return False
        return True

    def disconnect(self):
        """
        Disconnect from the device.
        @return: None
        """
        self.connection.disconnect()
        logging.info("Disconnected. host:{} logic:{}".format(self.host, self.name))


class GenericNetworkDevice(NetworkDevice):
    def set_name(self):
        self.name = "generic"
