import logging


def get_class_name(device_type):
    class_name = device_type.split("_")
    class_name = [c.capitalize() for c in class_name]
    class_name = "".join(class_name)
    return class_name + "NetworkDevice"


def get_driver_by_device_type(device_type):

    # dynamic import: try to find a corresponding module with extended
    # logic in devices/*.py via string device_type and import it

    try:
        class_name = get_class_name(device_type)
        logging.debug(
            "Import class '{}' from module 'mimir.devices.{}'".format(
                class_name, device_type
            )
        )
        module_name = __import__(
            "mimir.devices.{}".format(device_type), fromlist=[class_name]
        )
        target_class = getattr(module_name, class_name)
        logging.debug(
            "Extended logic for device type '{}' imported.".format(device_type)
        )
    except (ImportError, AttributeError) as e:
        logging.debug(
            "Extended logic for device type '{}' not found: {}".format(
                device_type, str(e)
            )
        )
        logging.debug("Falling back to 'generic' logic.")
        class_name = "GenericNetworkDevice"
        module_name = __import__("mimir.devices.generic", fromlist=[class_name])
        target_class = getattr(module_name, class_name)

    return target_class
